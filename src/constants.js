export default {
  MAIL_REGULAR_EXPRESSION: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  State: {
    AVALIDER: 'AVALIDER',
    ENCOURS: 'ENCOURS',
    REFUSE: 'REFUSE',
    VALIDE: 'VALIDE',
    TERMINE: 'TERMINE'
  }
}
