import moment from 'moment';

export default {
  formatDateFr(dateEn) {
    return moment(dateEn, 'YYYY-MM-DD').format('DD/MM/YYYY');
  }
}
