import Vuex from 'vuex';
import Vue from 'vue';
import car from "@/store/modules/car.js";
import user from "@/store/modules/user.js";
import auth from "@/store/modules/auth.js";
import app from "@/store/modules/app.js";
import borrowing from "@/store/modules/borrowing.js";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    car,
    user,
    auth,
    app,
    borrowing
  },
})

export default store;
