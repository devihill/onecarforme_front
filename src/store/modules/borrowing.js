import BorrowingService from "@/services/borrowing.service";

export default {
  namespaced: true,

  state: {
    borrowingList: [],
    userBorrowingList: [],
    currentBorrowing: null,
  },

  getters: {
    getBorrowingList: state => {
      return state.borrowingList;
    },
    getUserBorrowingList: state => {
      return state.userBorrowingList;
    },
    getCurrentBorrowing: state => {
      return state.currentBorrowing;
    },
    getCurrentBorrowingState: state => {
      return state.currentBorrowing.state;
    },
  },

  mutations: {
    setBorrowingList(state, value) {
      state.borrowingList = value;
    },
    setCurrentBorrowing(state, value) {
      state.currentBorrowing = value;
    },
    setUserBorrowingList(state, value) {
      state.userBorrowingList = value;
    },
    // Setters currentBorrowing
    setCar(state, value) {
      state.currentBorrowing.car = value;
    },
    setUser(state, value) {
      state.currentBorrowing.user = value;
    },
    setStartDate(state, value) {
      state.currentBorrowing.startDate = value;
    },
    setEndDate(state, value) {
      state.currentBorrowing.endDate = value;
    },
    setPickupAddress(state, value) {
      state.currentBorrowing.pickupAddress = value;
    },
    setReturnAddress(state, value) {
      state.currentBorrowing.returnAddress = value;
    },
    setState(state, value) {
      state.currentBorrowing.state = value;
    },
    setReason(state, value) {
      state.currentBorrowing.reason = value;
    },
    setComment(state, value) {
      state.currentBorrowing.comment = value;
    },
    setRefusedReason(state, value) {
      state.currentBorrowing.refusedReason = value;
    },
    setIsValid(state, value) {
      state.currentBorrowing.valid = value;
    },
  },

  actions: {
    collectCurrentBorrowing({
      commit
    }, id) {
      return new Promise(function (resolve, reject) {
        BorrowingService.getBorrowing(id).then(
          response => {
            commit("setCurrentBorrowing", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    },
    saveCurrentBorrowing({
      commit,
      state
    }) {
      return new Promise(function (resolve, reject) {
        BorrowingService.saveBorrowing(state.currentBorrowing).then(
          response => {
            commit("setCurrentBorrowing", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    },
    collectBorrowingsList({
      commit
    }) {
      return new Promise(function (resolve, reject) {
        BorrowingService.getAllBorrowings().then(
          response => {
            commit("setBorrowingList", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    },
    collectUserBorrowingsList({
      commit
    }, idUser) {
      return new Promise(function (resolve, reject) {
        BorrowingService.getAllUserBorrowings(idUser).then(
          response => {
            commit("setUserBorrowingList", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    },
  }
}
