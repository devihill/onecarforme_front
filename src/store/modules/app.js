export default {
  namespaced: true,

  state: {
    drawer: false,
    flash_message: {
      message: '',
      type: 'info',
      value: false
    },
  },

  getters: {
    getDrawer: state => {
      return state.drawer;
    },
  },

  mutations: {
    setFlashMessage(state, flash_message) {
      state.flash_message = flash_message;
      window.scrollTo(0, 0);
      setTimeout(function () {
        state.flash_message.value = false
      }, 5000); //clears msg in 5 seconds
    },
    changeDrawer(state) {
      state.drawer = !state.drawer;
    },
    setDrawer(state, value) {
      state.drawer = value;
    },
  },

  actions: {}

}