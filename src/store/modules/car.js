import CarService from "@/services/car.service";
export default {
  namespaced: true,

  state: {
    carsList: [],
    currentCar: null,
  },

  getters: {
    getCarsList: state => {
      return state.carsList;
    },
    getCurrentCar: state => {
      return state.currentCar;
    },
  },

  mutations: {
    setCarsList(state, value) {
      state.carsList = value;
    },
    setCurrentCar(state, value) {
      state.currentCar = value;
    },
    // Setters currentCar
    setRegistrationNumber(state, value) {
      state.currentCar.registrationNumber = value;
    },
    setSeat(state, value) {
      state.currentCar.seat = value;
    },
    setModel(state, value) {
      state.currentCar.model = value;
    },
    setMark(state, value) {
      state.currentCar.mark = value;
    },
    setGearBox(state, value) {
      state.currentCar.gearBox = value;
    },
    setFuel(state, value) {
      state.currentCar.fuel = value;
    },
    setMileage(state, value) {
      state.currentCar.mileage = value;
    },
  },

  actions: {
    collectCurrentCar({ commit }, id) {
      return new Promise(function (resolve, reject) {
        CarService.getCar(id).then(
          response => {
            commit("setCurrentCar", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    },
    collectCarsList({ commit }, id) {
      return new Promise(function (resolve, reject) {
        CarService.getListCars().then(
          response => {
            commit("setCarsList", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    },
    saveCurrentCar({ commit, state }) {
      return new Promise(function (resolve, reject) {
        CarService.saveCar(state.currentCar).then(
          response => {
            commit("setCurrentCar", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    }
  }
}
