import userService from "@/services/user.service"

export default {
  namespaced: true,

  state: {
    authenticatedUser: null,
    isAuthenticated: false,
  },

  getters: {
    isAuthenticated: state => {
      return state.isAuthenticated;
    },
    getAuthenticatedUser: state => {
      return state.authenticatedUser
    }
  },

  mutations: {
    setAuthenticated(state, value) {
      state.isAuthenticated = value;
    },
    setAuthenticatedUser(state, value) {
      state.authenticatedUser = value;
    }
  },

  actions: {
    /**
     * Récupération de l'utilisateur connecté dans le store
     */
    async collectAuthenticatedUser({ commit }) {
      return new Promise(function (resolve, reject) {
        userService.getConnectedUser().then(response => {
          commit('setAuthenticatedUser', response.data);
          resolve();

        }).catch(error => {
          reject(error);
        });
      });
    }
  }
}
