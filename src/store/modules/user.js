import UserService from "@/services/user.service";

export default {
  namespaced: true,

  state: {
    usersList: [],
    currentUser: null,
  },

  getters: {
    getUsersList: state => {
      return state.usersList;
    },
    getCurrentUser: state => {
      return state.currentUser;
    }
  },

  mutations: {
    setUsersList(state, value) {
      state.usersList = value;
    },
    setCurrentUser(state, value) {
      state.currentUser = value;
    },
    // Setters currentUser
    setLogin(state, value) {
      state.currentUser.login = value;
    },
    setFirstName(state, value) {
      state.currentUser.firstName = value;
    },
    setLastName(state, value) {
      state.currentUser.lastName = value;
    },
    setEmail(state, value) {
      state.currentUser.email = value;
    },
    setCity(state, value) {
      state.currentUser.city = value;
    },
    setCp(state, value) {
      state.currentUser.cp = value;
    },
    setAuthorities(state, value) {
      state.currentUser.authorities = value;
    },
  },

  actions: {
    collectCurrentUser({ commit }, login) {
      return new Promise(function (resolve, reject) {
        UserService.getUser(login).then(
          response => {
            commit("setCurrentUser", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    },
    collectUsersList({ commit }) {
      return new Promise(function (resolve, reject) {
        UserService.getListUsers().then(
          response => {
            commit("setUsersList", response.data);
            resolve();
          }
        ).catch(
          error => {
            reject(error);
          }
        )
      })
    },
  }
}
