// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css'
import App from './App';
import router from '@/router.js';
import store from '@/store/index'

Vue.use(Vuetify, {
  theme: {
    colorOne: '#05668D',
    colorTwo: '#028090',
    colorThree: '#00A896',
    colorFour: '#02C39A',
    colorFive: '#F0F3BD'
  },
  options: {
    customProperties: true
  }
});
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
