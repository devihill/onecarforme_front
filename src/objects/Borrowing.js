export default class Borrowing {

  constructor() {
    this.id = 0,
      this.car = null,
      this.user = null,
      this.startDate = null,
      this.endDate = null,
      this.pickupAddress = null,
      this.returnAddress = null,
      this.state = null,
      this.reason = null,
      this.comment = null,
      this.refusedReason = null,
      this.valid = null
  }

}
