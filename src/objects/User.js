export default class User {

  constructor() {
    this.login = "";
    this.last_name = "";
    this.email = "";
    this.cp = null;
    this.city = null;
    this.authorities = [];
  }
}
