export default class Car {
  constructor() {
    this.registrationNumber = null;
    this.seat = null;
    this.model = null;
    this.mark = null;
    this.gearBox = null;
    this.fuel = null;
    this.mileage = null;
  }
}
