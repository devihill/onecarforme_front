import Vue from 'vue'
import Router from 'vue-router'

// Services
import authenticateService from '@/services/authenticate.service'
import initService from '@/services/init.service'
import checkPathService from '@/services/checkpath.service'

// Pages
import AuthenticationPage from '@/pages/AuthenticationPage'
import NotFoundPage from '@/pages/NotFoundPage'
import UserListPage from '@/pages/UserListPage'
import SaveUserPage from '@/pages/SaveUserPage'
import CarListPage from '@/pages/CarListPage'
import SaveCarPage from '@/pages/SaveCarPage'
import BorrowingListPage from '@/pages/BorrowingListPage'
import CreateBorrowingPage from "@/pages/CreateBorrowingPage"
import RequestBorrowingPage from "@/pages/RequestBorrowingPage"
import MyBorrowingsPage from "@/pages/MyBorrowingsPage"

Vue.use(Router)


const router = new Router({
  routes: [{
    path: '*',
    name: 'NotFound',
    component: NotFoundPage,
    meta: {
      requiresAuth: true // Permet de savoir si l'authentification est nécessaire pour atteindre la page
    }
  },
  {
    path: '/',
    name: 'Authentication',
    component: AuthenticationPage,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/cars-list',
    name: 'CarList',
    component: CarListPage,
    meta: {
      requiresAuth: true
    },
    beforeEnter: checkPathService.checkCarList
  },
  {
    path: '/save-car/:id?',
    name: 'SaveCarPage',
    component: SaveCarPage,
    meta: {
      requiresAuth: true
    },
    beforeEnter: checkPathService.checkSaveCar
  },
  {
    path: '/users',
    name: 'UserList',
    component: UserListPage,
    meta: {
      requiresAuth: true
    },
    beforeEnter: checkPathService.checkUserList
  },
  {
    path: '/save-user/:login?',
    name: 'SaveUser',
    component: SaveUserPage,
    meta: {
      requiresAuth: true
    },
    beforeEnter: checkPathService.checkSaveUser
  },
  {
    path: '/borrowings',
    name: 'BorrowingList',
    component: BorrowingListPage,
    meta: {
      requiresAuth: true
    },
    beforeEnter: checkPathService.checkBorrowingListPage
  },
  {
    path: '/create-borrowing',
    name: 'CreateBorrowing',
    component: CreateBorrowingPage,
    meta: {
      requiresAuth: true
    },
    beforeEnter: checkPathService.checkCreateBorrowing
  },
  {
    path: '/request-borrowing/:id?',
    name: 'RequestBorrowingPage',
    component: RequestBorrowingPage,
    meta: {
      requiresAuth: true
    },
    beforeEnter: checkPathService.checkRequestBorrowingPage
  },
  {
    path: '/my-borrowings',
    name: 'MyBorrowingsPage',
    component: MyBorrowingsPage,
    meta: {
      requiresAuth: true
    },
    beforeEnter: checkPathService.checkMyBorrowingsPage
  },
  ]
});

// Vérification de certains critères avant d'atteindre la route suivante
router.beforeEach((to, from, next) => {
  // Récupération de la data essentielle au bon fonctionnement de l'application si elle n'est pas présente.
  initService.initData();
  // Vérification de l'authentification de l'utilisateur
  authenticateService.isAuthenticated().then(isAuthenticated => {
    if (to.matched.some(record => record.meta.requiresAuth) && !isAuthenticated) {
      // Si la route à atteindre a besoin d'une authentification et si l'utilisateur n'est pas connectée
      // Alors l'utilisateur est redirigé vers la page d'authentification
      next({
        path: '/'
      })
    } else if (isAuthenticated && to.path == '/') {
      if (authenticateService.isRoleAdmin()) {
        next({
          path: '/borrowings'
        })
      } else {
        next({
          path: '/my-borrowings'
        })
      }
    } else {
      // Sinon on va à la page suivante
      next()
    }
  })

})

export default router;
