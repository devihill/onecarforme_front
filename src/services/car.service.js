import http from '@/../config/http';

export default {
  saveCar(Car) {
    return http.post("/car", Car);
  },
  getListCars() {
    return http.get("/cars");
  },
  deleteCar(id) {
    return http.delete("/car/" + id);
  },
  editCar(car) {
    return http.post("/car/edit", car);
  },
  getCar(registrationNumber) {
    return http.get("/car/" + registrationNumber);
  },
  quickSearch(quickSearch) {
    return http.get("/car/quicksearch", {
      params: {
        quickSearch: quickSearch
      }
    });
  }
}
