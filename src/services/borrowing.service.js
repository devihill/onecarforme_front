import http from '@/../config/http';

export default {
  saveBorrowing(borrowing) {
    return http.post("/borrowing", borrowing);
  },
  getAllBorrowings() {
    return http.get("/borrowings");
  },
  getAllUserBorrowings(id) {
    return http.get("/user-borrowings/" + id);
  },
  getBorrowing(id) {
    return http.get("/borrowing/" + id);
  }
}
