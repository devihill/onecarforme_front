import http from '@/../config/http';

export default {
  createUser(userDTO) {
    return http.post('/users', userDTO)
  },
  modifyUser(userDTO) {
    return http.put('/users', userDTO)
  },
  updateUser(userDTO) {
    return http.put('/users', userDTO)
  },
  getListUsers() {
    return http.get("/users");
  },
  deleteUser(login) {
    return http.delete("/users/" + login);
  },
  getUser(login) {
    return http.get("/users/" + login)
  },
  getConnectedUser() {
    return http.get("/account")
  }
}