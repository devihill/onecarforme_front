import store from '@/store/index'
import authenticateService from '@/services/authenticate.service'

export default {
    async initData() {
        // Récupération de l'utilisateur dans le store si on ne l'a pas
        if (authenticateService.isAuthenticated && !store.getters['auth/getAuthenticatedUser']) {
            await store.dispatch('auth/collectAuthenticatedUser');
        }
    }
}