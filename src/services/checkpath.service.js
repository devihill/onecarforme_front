import authenticateService from '@/services/authenticate.service';

/**
 * Service gérant les droit d'accès de chaques routes de l'application, utilisé par le router
 */
export default {
    checkCarList(to, from, next) {
        if (authenticateService.isRoleAdmin()) {
            next();
        } else {
            next(false);
        }
    },

    checkSaveCar(to, from, next) {
        if (authenticateService.isRoleAdmin()) {
            next();
        } else {
            next(false);
        }
    },

    checkUserList(to, from, next) {
        if (authenticateService.isRoleAdmin()) {
            next();
        } else {
            next(false);
        }
    },

    checkSaveUser(to, from, next) {
        if (authenticateService.isRoleAdmin()) {
            next();
        } else {
            next(false);
        }
    },

    checkBorrowingListPage(to, from, next) {
        if (authenticateService.isRoleAdmin()) {
            next();
        } else {
            next(false);
        }
    },

    checkCreateBorrowing(to, from, next) {
        if (authenticateService.isRoleAdmin()) {
            next();
        } else {
            next(false);
        }
    },

    checkRequestBorrowingPage(to, from, next) {
        next();
    },

    checkMyBorrowingsPage(to, from, next) {
        next();
    },

}