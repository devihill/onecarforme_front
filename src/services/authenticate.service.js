import http from '@/../config/http';
import store from '@/store/index';

const tokenKey = 'Authorization';

export default {

  /**
   * Méthode asynchrone de récupération du token de connexion utilisateur à partir du back
   * 
   * @param {*} LoginVM Objet de connexion utilisateur (login, mdp)
   */
  async authenticateUser(LoginVM) {
    // On vérifie si l'utilisateur n'est pas déjà connecté
    var isAuthenticated = await this.isAuthenticated();
    if (!isAuthenticated) {
      try {
        // Authentification ver le back
        var response = await http.post("/authenticate", LoginVM);
        // Stockage du token dans le navigateur
        if (LoginVM.rememberMe) {
          localStorage.setItem(tokenKey, response.headers.authorization);
        } else {
          sessionStorage.setItem(tokenKey, response.headers.authorization);
        }
        store.commit('auth/setAuthenticated', true);
        return true;
      } catch (ex) {
        console.error(ex);
        return false;
      }
    }
  },

  /**
   * Méthode de récupération du token sauvegardé dans le navigateur
   */
  getSavedAuthorizationToken() {
    var token = null;
    if (localStorage[tokenKey]) {
      token = localStorage[tokenKey];
    } else if (sessionStorage[tokenKey]) {
      token = sessionStorage[tokenKey];
    }
    return token;
  },

  /**
   * Méthode de vérification de l'authentification de l'utilisateur
   */
  isAuthenticated() {
    var instance = this;
    return new Promise(function (resolve, reject) {
      // Si l'utilisateur est déjà authentifié on retourne true
      if (store.getters.isAuthenticated) {
        resolve(true);
      }
      // Sinon on vérifie qu'un token a été stocké et on vérifie sa validité
      else {
        var token = instance.getSavedAuthorizationToken();
        if (!token) {
          resolve(false);
        } else {
          http.get('/authenticate').then(response => {
            // Si la réponse est 200 on valide l'authentification
            if (response.status === 200) {
              store.commit('auth/setAuthenticated', true);
              resolve(true);
            } else {
              resolve(false);
            }
          }).catch(ex => {
            // Si le token n'est pas valide on le supprime
            instance.clearToken();
            resolve(false);
          });
        }
      }
    })
  },

  /**
   * Méthode de suppression du token stocké
   */
  clearToken() {
    localStorage.removeItem(tokenKey);
    sessionStorage.removeItem(tokenKey);
    store.commit('auth/setAuthenticated', false);
    this.clearStore();
  },

  /**
   * Méthode pour nettoyer certains states
   */
  clearStore() {
    store.commit('auth/setAuthenticatedUser', null)
  },

  /**
   * Méthode permettant de savoir si l'utilisateur connecté possède le role user
   */
  isRoleUser() {
    if (store.getters['auth/getAuthenticatedUser'] != null) {
      return store.getters['auth/getAuthenticatedUser'].authorities.indexOf(process.env.ROLES.user) > -1;
    } else {
      return false;
    }
  },

  /**
   * Méthode permettant de savoir si l'utilisateur connecté possède le role admin
   */
  isRoleAdmin() {
    if (store.getters['auth/getAuthenticatedUser'] != null) {
      return store.getters['auth/getAuthenticatedUser'].authorities.indexOf(process.env.ROLES.admin) > -1;
    } else {
      return false;
    }
  }

}
