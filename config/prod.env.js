'use strict'
module.exports = {
  NODE_ENV: '"production"',
  URL_BACK: '"http://5.196.89.184:8090/api"',
  ROLES: {
    admin: '"ROLE_ADMIN"',
    user: '"ROLE_USER"'
  }
}
