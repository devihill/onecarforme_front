'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  URL_BACK: '"http://localhost:8090/api"',
  ROLES: {
    admin: '"ROLE_ADMIN"',
    user: '"ROLE_USER"'
  }
})
