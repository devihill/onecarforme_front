import axios from 'axios'
import authenticateService from '@/services/authenticate.service'

// Création de l'instance axios
const http = axios.create({
    baseURL: process.env.URL_BACK,
})

// Ajout d'un intercepteur pour rajouter le token de connexion dans le header
// si il existe
http.interceptors.request.use(
    (config) => {
        let token = authenticateService.getSavedAuthorizationToken();
        if (token) {
            config.headers.Authorization = `${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error)
    }
)

export default http;